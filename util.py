import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import cm


def read_image(path):
    image = mpimg.imread(path)
    return image


def copy_image(image):
    color_select = np.copy(image)
    return color_select


def display_and_save_image(img, path):
    plt.imshow(img)
    mpimg.imsave(path, img)


# grayscale conversion
def gray_out_image(image):
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    return gray


def histeq(image):
    image = cv2.equalizeHist(image)
    return image


def canned_image(grayedImage, kernel_size=5, low_threshold=50, high_threshold=150):
    # Define a kernel size for Gaussian smoothing / blurring
    blur_gray = cv2.GaussianBlur(grayedImage, (kernel_size, kernel_size), 0)
    edges = cv2.Canny(blur_gray, low_threshold, high_threshold)
    edges = cv2.GaussianBlur(edges, (kernel_size, kernel_size), 0)
    return edges


def hough_transform_and_region_select(image, edges):
    rho = 1
    theta = np.pi/180
    threshold = 500
    min_line_length = 50
    max_line_gap = 3

    # Next we'll create a masked edges image using cv2.fillPoly()
    mask = np.zeros_like(edges)
    ignore_mask_color = 255

    # This time we are defining a four sided polygon to mask
    imshape = image.shape
    vertices = np.array([[(0, imshape[0]), (0, imshape[0]/2), (imshape[1], imshape[0]/2), (imshape[1], imshape[0])]],
                        dtype=np.int32)
    cv2.fillPoly(mask, vertices, ignore_mask_color)
    masked_edges = cv2.bitwise_and(edges, mask)
    display_and_save_image(edges, "edges.jpg")
    masked_edges = edges

    line_image = np.copy(image)*0

    # Run Hough on edge detected image
    # Output "lines" is an array containing endpoints of detected line segments
    lines = cv2.HoughLinesP(masked_edges, rho=rho, theta=theta, threshold=threshold, lines=np.array([]),
                            minLineLength=min_line_length, maxLineGap=max_line_gap)

    # Iterate over the output "lines" and draw lines on a blank image
    angles = []
    bins = np.linspace(start=-90, stop=90, num=20)
    viridis = cm.get_cmap('viridis', 20)
    viridis(np.linspace(0, 255, 9))
    for line in lines:
        for x1, y1, x2, y2 in line:
            line_angle = np.arctan((y2 - y1) / (x2 - x1))/np.pi * 180
            angles.append(line_angle)
    hist = np.histogram(angles, bins=bins)
    important_bins = [item[0] for item in
                      sorted(np.stack((hist[1][:-1], hist[0]), 1), key=lambda element: element[1], reverse=True)[0:3]]
    inds = np.digitize(angles, bins)
    for index in range(len(lines)):
        for x1, y1, x2, y2 in lines[index]:
            if bins[inds[index]-1] in important_bins:  # 4, 5, 6
                cv2.line(line_image, (x1, y1), (x2, y2), tuple([int(item) for item in viridis.colors[inds[index]-1][:-1] * 255]), 10)

    # Create a "color" binary image to combine with line image
    color_edges = np.dstack((edges, edges, edges))

    # Draw the lines on the edge image
    lines_edges = cv2.addWeighted(color_edges, 0.8, line_image, 1, 0)

    return lines_edges
