import cv2
import numpy as np
import matplotlib.pyplot as plt


img = cv2.imread('dataset/6.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 150, 250, apertureSize=3)

edges = cv2.GaussianBlur(edges, (3, 3), 0)

cv2.imwrite('result0.jpg', edges)


# lines = cv2.HoughLines(edges, 1, np.pi/180, 300)
# for item in lines:
#     rho = item[0][0]
#     theta = item[0][1]
#     a = np.cos(theta)
#     b = np.sin(theta)
#     x0 = a * rho
#     y0 = b * rho
#     x1 = int(x0 + 1000 * (-b))
#     y1 = int(y0 + 1000 * a)
#     x2 = int(x0 - 1000 * (-b))
#     y2 = int(y0 - 1000 * a)
#
#     cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 5)
#
# cv2.imwrite('result.jpg', img)




minLineLength = 100
maxLineGap = 10
lines = cv2.HoughLinesP(edges, 1, np.pi/180, 300, minLineLength, maxLineGap)
for item in lines:
    x1 = item[0][0]
    y1 = item[0][1]
    x2 = item[0][2]
    y2 = item[0][3]
    cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 5)


cv2.imwrite('houghlines5.jpg', img)