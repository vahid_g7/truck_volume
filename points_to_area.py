

# based on https://stackoverflow.com/questions/20677795/how-do-i-compute-the-intersection-point-of-two-lines
def find_line_intersection(line1, line2):

    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        raise Exception('lines do not intersect')

    d = (det(line1[0], line1[1]), det(line2[0], line2[1]))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y


def find_area(point_a, point_b, point_c, point_d):
    vanishing_point = find_line_intersection(line1=[point_a, point_b], line2=[point_c, point_d])

