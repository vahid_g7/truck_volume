import util
import points_to_area


def find_lines(image, name):
    # Make a copy of image
    grayed_select = util.copy_image(image)

    # Generate Gray scaled image
    grayed_select = util.gray_out_image(grayed_select)

    # Histogram equalization
    equalized_image = util.histeq(grayed_select)

    # Applied Canny function to grayed scale image to find out edges of gradiants
    edges = util.canned_image(equalized_image)

    hough_transformed_select = util.hough_transform_and_region_select(image, edges)
    util.display_and_save_image(hough_transformed_select, name)


image = util.read_image('dataset/6.jpg')

find_lines(image, 'result.jpg')

a = [2, 2]
b = [1, 1]
c = [4, 1]
d = [3, 2]
points_to_area.find_area(point_a=a, point_b=b, point_c=c, point_d=d)